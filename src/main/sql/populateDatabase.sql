INSERT INTO user_1 VALUES (default,'robert', 'password', 'robert.rebacz@gmail.com');
INSERT INTO user_1 VALUES (default,'wika', 'password', 'wika@gmail.com');
INSERT INTO user_1 VALUES (default,'bob', 'password', 'bob@gmail.com');

INSERT INTO goal VALUES (default, '2018-02-17' ,'2018-02-28', 'goal description for goal nr 1', 'goal nr 1 name',1);
INSERT INTO goal VALUES (default, '2018-02-17' ,'2018-03-15', 'goal description for goal nr 2', 'goal nr 2 name',1);
INSERT INTO goal VALUES (default, '2018-02-17' ,'2018-03-25', 'goal description for goal nr 3', 'goal nr 3 name',1);

INSERT INTO task VALUES (default,'task name nr 1','task description for task nr 1', '2018-02-17' ,'2018-03-10', 3);
INSERT INTO task VALUES (default,'task name nr 2','task description for task nr 2', '2018-02-17' ,'2018-03-15', 3);
INSERT INTO task VALUES (default,'task name nr 1','task description for task nr 1', '2018-02-17' ,'2018-03-17', 4);
INSERT INTO task VALUES (default,'task name nr 2','task description for task nr 2', '2018-02-17' ,'2018-03-20', 4);

