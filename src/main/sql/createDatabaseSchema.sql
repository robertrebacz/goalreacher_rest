CREATE SEQUENCE user_1_userid_seq;

CREATE TABLE user_1 (
                userid INTEGER NOT NULL DEFAULT nextval('user_1_userid_seq'),
                username VARCHAR(15) NOT NULL,
                passwaord VARCHAR(15) NOT NULL,
                email VARCHAR(50) NOT NULL,
                CONSTRAINT userid PRIMARY KEY (userid)
);


ALTER SEQUENCE user_1_userid_seq OWNED BY user_1.userid;

CREATE SEQUENCE goal_goalid_seq;

CREATE TABLE goal (
                goalid INTEGER NOT NULL DEFAULT nextval('goal_goalid_seq'),
                goalcreatedate DATE NOT NULL,
                goaltargetdate DATE NOT NULL,
                goaldescription VARCHAR(100) NOT NULL,
                goalname VARCHAR(20) NOT NULL,
                userid INTEGER NOT NULL,
                CONSTRAINT goalid PRIMARY KEY (goalid)
);


ALTER SEQUENCE goal_goalid_seq OWNED BY goal.goalid;

CREATE SEQUENCE task_taskid_seq;

CREATE TABLE task (
                taskid INTEGER NOT NULL DEFAULT nextval('task_taskid_seq'),
                taskname VARCHAR(20) NOT NULL,
                taskdescription VARCHAR(100) NOT NULL,
                taskcreatedate DATE NOT NULL,
                tasktargetdate DATE NOT NULL,
                goalid INTEGER NOT NULL,
                CONSTRAINT taskid PRIMARY KEY (taskid)
);


ALTER SEQUENCE task_taskid_seq OWNED BY task.taskid;

ALTER TABLE goal ADD CONSTRAINT user_goal_fk
FOREIGN KEY (userid)
REFERENCES user_1 (userid)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE task ADD CONSTRAINT goal_task_fk
FOREIGN KEY (goalid)
REFERENCES goal (goalid)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;