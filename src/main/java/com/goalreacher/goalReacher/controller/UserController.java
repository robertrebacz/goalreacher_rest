package com.goalreacher.goalReacher.controller;

import com.goalreacher.goalReacher.model.User;
import com.goalreacher.goalReacher.service.User.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/REST/User")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("allUsers")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }
}
