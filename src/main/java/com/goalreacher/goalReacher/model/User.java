package com.goalreacher.goalReacher.model;

import javax.persistence.*;

@Entity
@Table(name = "user_1")
public class User {
    @Id
    @GeneratedValue
    @Column(name = "userid")
    private int userId;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "email")
    private String email;

    public int getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }
}
