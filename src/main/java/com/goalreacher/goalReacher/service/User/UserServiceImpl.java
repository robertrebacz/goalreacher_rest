package com.goalreacher.goalReacher.service.User;

import com.goalreacher.goalReacher.model.User;
import com.goalreacher.goalReacher.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{
    @Autowired
    private UserRepository userRepository;

    public List<User> getAllUsers(){
//        System.out.println("getALLuSERS");
//        User user = userRepository.findOne(1);
//        System.out.println(user.getUsername());

        return userRepository.findAll();
    }
}
