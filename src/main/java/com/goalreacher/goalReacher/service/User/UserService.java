package com.goalreacher.goalReacher.service.User;

import com.goalreacher.goalReacher.model.User;

import java.util.List;

public interface UserService {
    List<User> getAllUsers();
}
