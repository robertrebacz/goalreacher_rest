package com.goalreacher.goalReacher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@EnableJpaRepositories(basePackages = "com.goalreacher.goalReacher.repository")
@EnableJpaRepositories(basePackages = "com.goalreacher.goalReacher.repository")
@SpringBootApplication

public class GoalReacherApplication {

	public static void main(String[] args) {
		SpringApplication.run(GoalReacherApplication.class, args);
	}
}
